import Apps from './views/Apps.vue'
import AppsIndex from './views/AppsIndex.vue';
import AppsCreate from './views/AppsCreate.vue';
import AppsCreateJS from './views/AppsCreateJS.vue';
import AppsCreateHTML from './views/AppsCreateHTML.vue';

export const appsRoutes = {
    path: '/apps',
    component: Apps,
    props: true,
    children: [
        {
            path: '',
            name: 'index',
            component: AppsIndex,
        },
        {
            path: 'create',
            name: 'create',
            component: AppsCreate,
            children: [
                {
                    path: 'js',
                    component: AppsCreateJS
                },
                {
                    path: 'html',
                    component: AppsCreateHTML
                },
                {
                    path: '*',
                    redirect: '/apps/create/js'
                }
            ]
        }
    ]
};