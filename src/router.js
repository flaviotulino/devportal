import Vue from 'vue'
import Router from 'vue-router'
import {appsRoutes} from "./apps.routes";


Vue.use(Router);

export default new Router({
    routes: [
        appsRoutes,
        {
            path: '/sites',
            name: 'sites',
            component: () => import('./views/Sites.vue')
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '*',
            redirect: '/apps'
        }
    ]
})
