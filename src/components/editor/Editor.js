import {v4} from 'node-uuid';

require("ace-builds/webpack-resolver");
import * as ace from 'ace-builds/src-noconflict/ace';

const SUPPORTED_LANGUAGES = [
    'javascript',
    'html'
];

export default {
    name: 'Editor',
    props: {
        lang: {
            type: String,
            required: true,
            validator: function (value) {
                return SUPPORTED_LANGUAGES.includes(value);
            }
        }

    },
    computed: {
        editorId() {
            return 'editorId-' + v4();
        }
    },
    mounted() {
        const editor = ace.edit(this.editorId);
        editor.setTheme("ace/theme/monokai");

        editor.session.setMode(`ace/mode/${this.lang}`);
    }
};
