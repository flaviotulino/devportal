import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const CHANGE_NAME = 'CHANGE_NAME';

export default new Vuex.Store({
    state: {
        name: ''
    },
    mutations: {
        [CHANGE_NAME] (state, name) {
            state.name = name;
        }
    },
    actions: {
        [CHANGE_NAME]({commit}, name) {
            commit(CHANGE_NAME, name);
        }
    }
})
